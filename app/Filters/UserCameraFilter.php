<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Session;

class UserCameraFilter extends AbstractFilter
{
    public const BINO = 'bino';
    public const QAVAT = 'qavat';
    public const XONA = 'xona';
    public const TITLE = 'title';

    protected function getCallbacks(): array
    {
        return [
            self::BINO => [$this, 'bino'],
            self::QAVAT => [$this, 'qavat'],
            self::XONA => [$this, 'xona'],
            self::TITLE => [$this, 'title'],
        ];
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return void
     */
    public function bino(Builder $builder, $value)
    {
        $builder->where('bino', '=', $value)
            ->where('rahbarlar->id->rahbar', Session::get('id'))
            ->where('rahbarlar->id->rector', Session::get('id'))
            ->where('rahbarlar->id->rahbar_sub', Session::get('id'));
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return void
     */
    public function qavat(Builder $builder, $value)
    {
        $builder->where('qavat', '=', $value)
            ->where('rahbarlar->id->rahbar', Session::get('id'))
            ->where('rahbarlar->id->rector', Session::get('id'))
            ->where('rahbarlar->id->rahbar_sub', Session::get('id'));
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return void
     */
    public function xona(Builder $builder, $value)
    {
        $builder->where('xonalar', '=', $value)
            ->where('rahbarlar->id->rahbar', Session::get('id'))
            ->where('rahbarlar->id->rector', Session::get('id'))
            ->where('rahbarlar->id->rahbar_sub', Session::get('id'));
    }

    /**
     * @param Builder $builder
     * @param $value
     * @return void
     */
    public function title(Builder $builder, $value)
    {
        $builder->where('title', 'like', '%' . $value . '%')
            ->where('rahbarlar->id->rahbar', Session::get('id'))
            ->where('rahbarlar->id->rector', Session::get('id'))
            ->where('rahbarlar->id->rahbar_sub', Session::get('id'));
    }
}
