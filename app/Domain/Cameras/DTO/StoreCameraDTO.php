<?php

namespace App\Domain\Cameras\DTO;

class StoreCameraDTO
{
    /**
     * @var string
     */
    private string $title;

    /**
     * @var string
     */
    private string $link;

    /**
     * @var int|null
     */
    private ?int $section_id = null;

    /**
     * @var int|null
     */
    private ?int $favorite = null;

    /**
     * @var int|null
     */
    private ?int $sub_id = null;

    /**
     * @var int|null
     */
    private ?int $rahbar_id = null;

    /**
     * @var int|null
     */
    private ?int $xona_id = null;

    /**
     * @var string|null
     */
    private ?string $bino = null;

    /**
     * @var string|null
     */
    private ?string $qavat = null;

    /**
     * @var string|null
     */
    private ?string $xonalar = null;

    /**
     * @var string|null
     */
    private ?string $bolim_name = null;

    /**
     * @var string|null
     */
    private ?string $sub_bolim_name = null;

    /**
     * @var string|null
     */
    private ?string $local_ip = null;

    /**
     * @var array|null
     */
    private ?array $rahbarlar = null;

    /**
     * @param array $data
     * @return StoreCameraDTO
     */
    public static function fromArray(array $data): StoreCameraDTO
    {
        $dto = new self();
        $dto->setTitle($data['title']);
        $dto->setLink($data['link']);
        $dto->setSectionId($data['section_id'] ?? null);
        $dto->setFavorite($data['favorite'] ?? null);
        $dto->setBolimName($data['bolim_name'] ?? null);
        $dto->setSubBolimName($data['sub_bolim_name'] ?? null);
        $dto->setSubId($data['sub_id'] ?? null);
        $dto->setRahbarId($data['rahbar_id'] ?? null);
        $dto->setBino($data['bino'] ?? null);
        $dto->setQavat($data['qavat'] ?? null);
        $dto->setXonalar($data['xonalar'] ?? null);
        $dto->setXonaId($data['xona_id'] ?? null);
        $dto->setRahbarlar($data['rahbarlar'] ?? null);
        $dto->setLocalIp($data['local_ip'] ?? null);
        return $dto;
    }

    /**
     * @return string|null
     */
    public function getLocalIp(): ?string
    {
        return $this->local_ip;
    }

    /**
     * @param string|null $local_ip
     */
    public function setLocalIp(?string $local_ip): void
    {
        $this->local_ip = $local_ip;
    }

    /**
     * @return array|null
     */
    public function getRahbarlar(): ?array
    {
        return $this->rahbarlar;
    }

    /**
     * @param array|null $rahbarlar
     */
    public function setRahbarlar(?array $rahbarlar): void
    {
        $this->rahbarlar = $rahbarlar;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return int|null
     */
    public function getSectionId(): ?int
    {
        return $this->section_id;
    }

    /**
     * @param int|null $section_id
     */
    public function setSectionId(?int $section_id): void
    {
        $this->section_id = $section_id;
    }

    /**
     * @return int|null
     */
    public function getFavorite(): ?int
    {
        return $this->favorite;
    }

    /**
     * @param int|null $favorite
     */
    public function setFavorite(?int $favorite): void
    {
        $this->favorite = $favorite;
    }

    /**
     * @return int|null
     */
    public function getSubId(): ?int
    {
        return $this->sub_id;
    }

    /**
     * @param int|null $sub_id
     */
    public function setSubId(?int $sub_id): void
    {
        $this->sub_id = $sub_id;
    }

    /**
     * @return int|null
     */
    public function getRahbarId(): ?int
    {
        return $this->rahbar_id;
    }

    /**
     * @param int|null $rahbar_id
     */
    public function setRahbarId(?int $rahbar_id): void
    {
        $this->rahbar_id = $rahbar_id;
    }

    /**
     * @return int|null
     */
    public function getXonaId(): ?int
    {
        return $this->xona_id;
    }

    /**
     * @param int|null $xona_id
     */
    public function setXonaId(?int $xona_id): void
    {
        $this->xona_id = $xona_id;
    }

    /**
     * @return string|null
     */
    public function getBino(): ?string
    {
        return $this->bino;
    }

    /**
     * @param string|null $bino
     */
    public function setBino(?string $bino): void
    {
        $this->bino = $bino;
    }

    /**
     * @return string|null
     */
    public function getQavat(): ?string
    {
        return $this->qavat;
    }

    /**
     * @param string|null $qavat
     */
    public function setQavat(?string $qavat): void
    {
        $this->qavat = $qavat;
    }

    /**
     * @return string|null
     */
    public function getXonalar(): ?string
    {
        return $this->xonalar;
    }

    /**
     * @param string|null $xonalar
     */
    public function setXonalar(?string $xonalar): void
    {
        $this->xonalar = $xonalar;
    }

    /**
     * @return string|null
     */
    public function getBolimName(): ?string
    {
        return $this->bolim_name;
    }

    /**
     * @param string|null $bolim_name
     */
    public function setBolimName(?string $bolim_name): void
    {
        $this->bolim_name = $bolim_name;
    }

    /**
     * @return string|null
     */
    public function getSubBolimName(): ?string
    {
        return $this->sub_bolim_name;
    }

    /**
     * @param string|null $sub_bolim_name
     */
    public function setSubBolimName(?string $sub_bolim_name): void
    {
        $this->sub_bolim_name = $sub_bolim_name;
    }
}
