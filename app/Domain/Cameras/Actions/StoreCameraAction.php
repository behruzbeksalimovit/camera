<?php

namespace App\Domain\Cameras\Actions;

use App\Domain\Cameras\DTO\StoreCameraDTO;
use App\Domain\Cameras\Models\Camera;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StoreCameraAction
{
    /**
     * @param StoreCameraDTO $dto
     * @return Camera
     * @throws Exception
     */
    public function execute(StoreCameraDTO $dto): Camera
    {
        DB::beginTransaction();
        try {
            $camera = new Camera();
            $camera->xona_id = $dto->getXonaId();
            $camera->section_id = $dto->getSectionId();
            $camera->sub_id = $dto->getSubId();
            $camera->rahbar_id = $dto->getRahbarId();
            $camera->title = $dto->getTitle();
            $camera->link = $dto->getLink();
            $camera->favorite = $dto->getFavorite();
            $camera->bolim_name = $dto->getBolimName();
            $camera->sub_bolim_name = $dto->getSubBolimName();
            $camera->bino = $dto->getBino();
            $camera->qavat = $dto->getQavat();
            $camera->xonalar = $dto->getXonalar();
            $camera->rahbarlar = $dto->getRahbarlar();
            $camera->local_ip = $dto->getLocalIp();
            $camera->save();

        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $camera;
    }
}
