<?php

namespace App\Domain\Cameras\Actions;

use App\Domain\Cameras\DTO\StoreCameraDTO;
use App\Domain\Cameras\DTO\UpdateCameraDTO;
use App\Domain\Cameras\Models\Camera;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UpdateCameraAction
{
    /**
     * @param UpdateCameraDTO $dto
     * @return Camera
     * @throws Exception
     */
    public function execute(UpdateCameraDTO $dto): Camera
    {
        DB::beginTransaction();
        try {
            $camera = $dto->getCamera();
            $camera->xona_id = $dto->getXonaId() ?? $dto->getCamera()->xona_id;
            $camera->section_id = $dto->getSectionId() ?? $dto->getCamera()->section_id;
            $camera->sub_id = $dto->getSubId() ?? $dto->getCamera()->sub_id;
            $camera->rahbar_id = $dto->getRahbarId() ?? $dto->getCamera()->rahbar_id;
            $camera->title = $dto->getTitle() ?? $dto->getCamera()->title;
            $camera->link = $dto->getLink() ?? $dto->getCamera()->link;
            $camera->favorite = $dto->getFavorite();
            $camera->bolim_name = $dto->getBolimName() ?? $dto->getCamera()->bolim_name;
            $camera->sub_bolim_name = $dto->getSubBolimName() ?? $dto->getCamera()->sub_bolim_name;
            $camera->bino = $dto->getBino() ?? $dto->getCamera()->bino;
            $camera->qavat = $dto->getQavat() ?? $dto->getCamera()->qavat;
            $camera->xonalar = $dto->getXonalar() ?? $dto->getCamera()->xonalar;
            $camera->rahbarlar = $dto->getRahbarlar() ?? $dto->getCamera()->rahbarlar;
            $camera->local_ip = $dto->getLocalIp();
            $camera->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $camera;
    }
}
