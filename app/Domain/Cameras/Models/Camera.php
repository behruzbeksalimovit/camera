<?php

namespace App\Domain\Cameras\Models;

use App\Models\Traits\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain\Cameras\Models\Camera
 *
 * @property int $id
 * @property string $uuid
 * @property int $section_id
 * @property int|null $sub_id
 * @property int|null $rahbar_id
 * @property int|null $xona_id
 * @property string $title
 * @property string $link
 * @property int|null $favorite
 * @property string|null $bino
 * @property string|null $qavat
 * @property string|null $xonalar
 * @property array|null $rahbarlar
 * @property string|null $bolim_name
 * @property string|null $sub_bolim_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Camera newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Camera newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Camera query()
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereBino($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereBolimName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereFavorite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereQavat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereRahbarId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereRahbarlar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereSubBolimName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereSubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereXonaId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera whereXonalar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Camera filter(\App\Filters\FilterInterface $filter)
 * @mixin \Eloquent
 */
class Camera extends Model
{
    use HasFactory, Filterable;

    protected $perPage = 8;

    protected $casts = [
        'rahbarlar' => 'json'
    ];
}
