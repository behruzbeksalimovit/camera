<?php
namespace App\Domain\Cameras\Repositories;

use App\Domain\Cameras\Models\Camera;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class CameraRepository
{
//    API
    /**
     * @param $uuid
     * @return Camera|Builder|Model|object|null
     */
    public function getCameraByUUID($uuid)
    {
        return Camera::query()
            ->select('id','title','uuid','link')
            ->where('uuid','=',$uuid)
            ->first();
    }

    /**
     * @return Collection|array
     */
    public function getAll(): Collection|array
    {
        return Camera::query()
            ->select('id','title','link')
            ->get();
    }
//    API

    /**
     * @param $filter
     * @return LengthAwarePaginator
     */
    public function getPaginate($filter): LengthAwarePaginator
    {
        return Camera::query()
            ->Filter($filter)
            ->orderByDesc('id')
            ->paginate();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getPaginateCameraFavorite(): LengthAwarePaginator
    {
        return Camera::query()
            ->select('id','bolim_name','sub_id','favorite')
            ->where('favorite',1)
            ->groupBy('sub_id','bolim_name')
            ->paginate();
    }

//    For user section
    public function getBySectionCamera($filter)
    {
        return Camera::query()
            ->Filter($filter)
            ->orWhere('rahbarlar->id->rahbar', Session::get('id'))
            ->orWhere('rahbarlar->id->rector', Session::get('id'))
            ->orWhere('rahbarlar->id->rahbar_sub', Session::get('id'))
            ->orderByDesc('id')
            ->paginate();
    }

    //web

    /**
     * @return Collection|array
     */
    public function getFavourite(): Collection|array
    {
        return Camera::query()
            ->where('favorite','=',1)
            ->get();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getCameraLocalIP(): LengthAwarePaginator
    {
        return Camera::query()
            ->select('id','local_ip','local_ip_check')
            ->paginate();
    }
}
