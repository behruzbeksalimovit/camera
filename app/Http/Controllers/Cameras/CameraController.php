<?php

namespace App\Http\Controllers\Cameras;

use App\Domain\Cameras\Actions\StoreCameraAction;
use App\Domain\Cameras\Actions\UpdateCameraAction;
use App\Domain\Cameras\DTO\StoreCameraDTO;
use App\Domain\Cameras\DTO\UpdateCameraDTO;
use App\Domain\Cameras\Models\Camera;
use App\Domain\Cameras\Repositories\CameraRepository;
use App\Filters\AdminCameraFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminFilterRequest;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CameraController extends Controller
{
    /**
     * @var mixed
     */
    public mixed $cameras;

    /**
     * @var mixed
     */
    public mixed $binolar;

    /**
     * @var mixed
     */
    public mixed $departments;

    /**
     * @param CameraRepository $cameraRepository
     * @throws GuzzleException
     */
    public function __construct(CameraRepository $cameraRepository)
    {
        $client = new Client();
        $this->cameras = $cameraRepository;
        $this->binolar = json_decode($client->request('POST', 'https://uniwork.buxdu.uz/api/binolar.asp')->getBody())->binolar;
        $this->departments = json_decode($client->request('POST', 'https://uniwork.buxdu.uz/api/departments.asp')->getBody())->departments;
    }

//---------------------------------------   admin dashboard start   ---------------------------------------
    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws BindingResolutionException
     */
    public function index(AdminFilterRequest $request)
    {
        $filter = app()->make(AdminCameraFilter::class, ['queryParams' => array_filter($request->validated())]);
        return view('dashboard.cameras.index', [
            'cameras' => $this->cameras->getPaginate($filter),
            'binolar' => $this->binolar,
        ]);
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        return view('dashboard.cameras.create', [
            'sub_nulls' => $this->departments,
            'binolar' => $this->binolar,
        ]);
    }

    /**
     * @param Request $request
     * @param StoreCameraAction $action
     * @return JsonResponse|RedirectResponse
     * @throws GuzzleException
     */
    public function store(Request $request, StoreCameraAction $action)
    {
        try {
            $request->validate([
                'title' => 'required',
                'link' => 'required',
                'sub_id' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return response()
                ->json([
                    'status' => false,
                    'message' => $validationException->getMessage(),
                    'data' => $validationException->validator->errors()
                ]);
        }

        try {
            $dto = StoreCameraDTO::fromArray($request->all());
            $action->execute($dto);
            return redirect()->route('cameras.index')->with('store', 'Camera muvaffaqiyatli qo\'shildi!');
        } catch (Exception $exception) {
            return redirect()->back();
        }
    }

    public function show(Camera $camera)
    {
        return view('dashboard.cameras.show', [
            'camera' => $camera
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Camera $camera
     * @return Factory|View
     * @throws GuzzleException
     */
    public function edit(Camera $camera)
    {
        $client = new Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $camera->sub_id);
        $response_binolar = $client->request('POST', 'https://uniwork.buxdu.uz/api/xonalar.asp?bino=' . $camera->bino . '&' . 'qavat=' . $camera->qavat);
        return view('dashboard.cameras.edit', [
            'camera' => $camera,
            'departments' => $this->departments,
            'xonalar' => json_decode($response_binolar->getBody())->xonalar,
            'sub_not_nulls' => json_decode($response_sub_id->getBody())->departments,
            'binolar' => $this->binolar
        ]);
    }

    /**
     * @param Request $request
     * @param Camera $camera
     * @param UpdateCameraAction $action
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function update(Request $request, Camera $camera, UpdateCameraAction $action)
    {
        try {
            $request->validate([
                'title' => 'required',
                'link' => 'required',
                'sub_id' => 'required'
            ]);
        } catch (ValidationException $validationException) {
            return redirect()->back();
        }

        try {
            $request->merge([
                'camera' => $camera
            ]);
            $dto = UpdateCameraDTO::fromArray($request->all());
            $action->execute($dto);
            return redirect()->route('cameras.index')->with('update', 'Camera muvaffaqiyatli o\'zgartirildi!');
        } catch (Exception $exception) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Camera $camera
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function destroy(Camera $camera)
    {
        $camera->delete();
        return redirect()->back()->with('delete', 'Camera muvaffaqiyatli o\'chirildi!');
    }

    public function ajaxGetRahbarId(Request $request)
    {
        $client = new Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments.asp');
        $departments = json_decode($response_sub_id->getBody())->departments;
        $arr_sub_not_null = [];

        for ($i = 0; $i < count($departments); $i++) {
            if ($departments[$i]->id == $request->id) {
                $arr_sub_not_null[$i] = $departments[$i];
            }
        }

        return response([
            'departments' => $arr_sub_not_null
        ], 201);
    }

    public function ajaxGetSubId(Request $request)
    {
        $client = new Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $request->sub_id);
        $departments = json_decode($response_sub_id->getBody())->departments;

        return response([
            'departments' => $departments
        ], 200);
    }

    public function ajaxXonalar(Request $request)
    {
        $client = new Client();
        $response_rahbar_id = $client->request('POST', "https://uniwork.buxdu.uz/api/xonalar.asp?bino=" . $request->bino . "&" . "qavat=" . $request->qavat);

        $binolar = json_decode($response_rahbar_id->getBody())->xonalar;

        return response([
            'binolar' => $binolar
        ], 201);
    }

//---------------------------------------   admin dashboard start   ---------------------------------------

//---------------------------------------   web start   ---------------------------------------
    public function favourite()
    {
        return view('web.favourite', [
            'cameras' => $this->cameras->getFavourite()
        ]);
    }

    public function showFavouriteCamera($uuid)
    {
        return view('web.show', [
            'camera' => Camera::query()->where('uuid', $uuid)->where('favorite', '=', 1)->first()
        ]);
    }

    public function showFavouriteCameras($bino,$xona)
    {
        return view('web.show', [
            'bino' => $bino,
            'xona' => $xona,
        ]);
    }

    public function showCamera($bino)
    {
        return view('web.camera', [
            'bino' => $bino
        ]);
    }

    public function getLocalIp()
    {
        return view('dashboard.cameras.localip', [
            'ips' => $this->cameras->getCameraLocalIP()
        ]);
    }

    public function checkping($camera_id)
    {
        $camera = Camera::find($camera_id);
        $pingResult = exec("ping -c 1 $camera->local_ip");
        if (str_contains($pingResult, 'loss')) {
            $camera->local_ip_check = 2;
        } else {
            $camera->local_ip_check = 1;
        }
        $camera->update();
        return redirect()->back();
    }

    public function checkAllPing()
    {
        $cameras = Camera::query()->select('id', 'local_ip')->get();

        for ($i = 0; $i < count($cameras); $i++) {
            $camera = Camera::query()->select('id','local_ip','local_ip_check')->find($cameras[$i]->id);
            $result = exec("ping -c 1 $camera->local_ip");
            if (str_contains($result, 'loss')) {
                $camera->local_ip_check = 2;
            } else {
                $camera->local_ip_check = 1;
            }
            $camera->update();
        }

        return redirect()->back()->with('success','Ping tekshirish muvaffaqiyatli yakunlandi!');
    }
//---------------------------------------   web end   ---------------------------------------

}
