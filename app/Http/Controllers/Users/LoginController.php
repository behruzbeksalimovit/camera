<?php

namespace App\Http\Controllers\Users;

use App\Domain\Cameras\Models\Camera;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function showLogin()
    {
        return view('dashboard.auth.login');
    }

    public function login(Request $request)
    {
        $client = new Client();
        $response = $client->request('POST', 'https://uniwork.buxdu.uz/api/login.asp', [
            'form_params' => [
                'login' => $request->login,
                'parol' => $request->parol,
                'avtorizatsiya' => 1,
            ]
        ]);
        $result_error = json_decode($response->getBody());

        if (isset($result_error->code) == false) {
            $result = json_decode($response->getBody())->id;
            $result_fio = json_decode($response->getBody())->fio;
            $result_department = json_decode($response->getBody())->department;
            Session::put('id', $result);
            Session::put('fio', $result_fio);
            Session::put('department', $result_department);

            $user = Camera::query()
                ->orWhere('rahbarlar->id->rahbar', Session::get('id'))
                ->orWhere('rahbarlar->id->rector', Session::get('id'))
                ->orWhere('rahbarlar->id->rahbar_sub', Session::get('id'))
                ->first();
            if ($user) {
                return redirect()->route('users.index');
            } else {
                return redirect()->back();
            }

        } else {
            $err = $result_error->error->message;
            return redirect()->back()->with("xato", $err);
        }
    }

    public function logout(Request $request)
    {
        $request->flush();
        Session::flush();

        return redirect()->route('users.loginPage');
    }
}
