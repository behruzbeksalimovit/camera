<?php

namespace App\Http\Controllers\Users;

use App\Domain\Cameras\Models\Camera;
use App\Domain\Cameras\Repositories\CameraRepository;
use App\Filters\AdminCameraFilter;
use App\Filters\UserCameraFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminFilterRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserCameraController extends Controller
{
    /**
     * @var mixed
     */
    public mixed $cameras;

    public mixed $binolar;

    public mixed $departments;

    /**
     * @param CameraRepository $cameraRepository
     * @throws GuzzleException
     */
    public function __construct(CameraRepository $cameraRepository)
    {
        $client = new Client();
        $this->cameras = $cameraRepository;
        $this->binolar = json_decode($client->request('POST','https://uniwork.buxdu.uz/api/binolar.asp')->getBody())->binolar;
        $this->departments = json_decode($client->request('POST','https://uniwork.buxdu.uz/api/departments.asp')->getBody())->departments;
    }

    /**
     * @param AdminFilterRequest $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws BindingResolutionException
     */
    public function index(AdminFilterRequest $request)
    {
        $filter = app()->make(UserCameraFilter::class, ['queryParams' => array_filter($request->validated())]);
        return view('users.layouts.index',[
            'cameras' => $this->cameras->getBySectionCamera($filter),
            'binolar' => $this->binolar
        ]);
    }

    /**
     * @param $uuid
     * @return Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function show($uuid)
    {
        $camera = Camera::where('uuid','=',$uuid)->first();
        if (isset($camera) && ($camera->orWhere('rahbarlar->id->rahbar', Session::get('id')) || $camera->orWhere('rahbarlar->id->rahbar_sub', Session::get('id')) || $camera->orWhere('rahbarlar->id->rector', Session::get('id')))) {
            return view('users.cameras.show',[
                'camera' => $camera
            ]);
        } else {
            return redirect()->back();
        }
    }
}
