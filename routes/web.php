<?php

use App\Http\Controllers\Cameras\CameraController;
use App\Http\Controllers\Users\LoginController;
use App\Http\Controllers\Users\UserCameraController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//---------------------------------------   admin group start   ---------------------------------------

Route::get('/login',[\App\Http\Controllers\Auth\LoginController::class,'login'])->name('login');
Route::post('/login',[\App\Http\Controllers\Auth\LoginController::class,'auth'])->name('auth.login');
Route::post('/logout',[\App\Http\Controllers\Auth\LoginController::class,'logout'])->name('logout');

Route::group(['prefix' => 'dashboard','middleware' => 'auth:sanctum'], function (){
    Route::get('/index', function (){
        return view('dashboard.index');
    })->name('dashboard');
    Route::get('cameras/index',[CameraController::class,'index'])->name('cameras.index');
    Route::get('cameras/create',[CameraController::class,'create'])->name('cameras.create');
    Route::post('cameras/store',[CameraController::class,'store'])->name('cameras.store');
    Route::get('cameras/{camera}/edit',[CameraController::class,'edit'])->name('cameras.edit');
    Route::get('cameras/{camera}/show',[CameraController::class,'show'])->name('cameras.show');
    Route::get('camera/localip',[CameraController::class,'getLocalIp'])->name('camera.localip');
    Route::get('camera/{camera_id}/localip',[CameraController::class,'checkping'])->name('camera.localip.check');
    Route::get('camera/checkall',[CameraController::class,'checkAllPing'])->name('camera.localip.checkAll');
    Route::put('cameras/{camera}',[CameraController::class,'update'])->name('cameras.update');
    Route::delete('cameras/{camera}',[CameraController::class,'destroy'])->name('cameras.delete');
});
Route::post('/ajax/rahbarId',[CameraController::class, 'ajaxGetRahbarId'])->name('ajax.getRahbarId');
Route::post('/ajax/getId',[CameraController::class, 'ajaxGetSubId'])->name('ajax.getSubId');
Route::post('/ajax/getBinolar',[CameraController::class, 'ajaxXonalar'])->name('ajax.xonalar');
//---------------------------------------   admin group end   ---------------------------------------


//---------------------------------------   users group start   ---------------------------------------

Route::group(['prefix' => 'users','as' => 'users.' ], function (){
    Route::get('/',[LoginController::class, 'showLogin'])->name('loginPage');
    Route::post('/user/login',[LoginController::class, 'login'])->name('login');
    Route::post('/user/logout',[LoginController::class, 'logout'])->name('logout');

    Route::get('/index',[UserCameraController::class,'index'])->name('index');
    Route::get('{uuid}/show',[UserCameraController::class,'show'])->name('camera.show');
});
//---------------------------------------   users group end   ---------------------------------------

Route::get('/',[CameraController::class,'favourite']);
Route::get('/show/{uuid}',[CameraController::class,'showFavouriteCamera'])->name('web.camera');

Route::get('/show/camera/{bino}',[CameraController::class,'showCamera'])->name('web.cameras');
Route::get('/show/{bino}/{xona}',[CameraController::class,'showFavouriteCameras'])->name('web.camera.fav');
