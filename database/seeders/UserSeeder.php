<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::create([
            'name' => 'Admin',
            'login' => 'admin',
            'password' => Hash::make('Bekjon6616'),
            'role' => 1
        ]);

        \App\Models\User::create([
            'name' => 'Admin',
            'login' => 'bekhruz',
            'password' => Hash::make('8008'),
            'role' => 1
        ]);

        \App\Models\User::create([
            'name' => 'Admin',
            'login' => 'api',
            'password' => Hash::make('faxri_go'),
            'role' => 0
        ]);
    }
}
