<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cameras', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('section_id');
            $table->unsignedBigInteger('sub_id')->nullable();
            $table->unsignedBigInteger('rahbar_id')->nullable();
            $table->unsignedBigInteger('xona_id')->nullable();
            $table->text('title');
            $table->text('link');
            $table->integer('favorite')->default(0)->nullable();
            $table->text('bino')->nullable();
            $table->text('qavat')->nullable();
            $table->text('xonalar')->nullable();
            $table->json('rahbarlar')->nullable();
            $table->text('bolim_name')->nullable();
            $table->text('sub_bolim_name')->nullable();
            $table->text('local_ip')->nullable();
            $table->integer('local_ip_check')->default(0)->comment('0 default, 1-true,2-false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cameras');
    }
};
