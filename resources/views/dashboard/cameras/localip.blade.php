@extends('dashboard.layouts.master')
@section('content')
    <div id="filter-card" class="card shadow-lg">
{{--        <form action="{{ route('cameras.index') }}" method="get">--}}
{{--            <div class="card-body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3">--}}
{{--                        <label for="bino">Bino</label>--}}
{{--                        <select name="bino" id="bino" class="form-control">--}}
{{--                            <option value="">Binoni tanlang!</option>--}}
{{--                            @foreach($binolar as $bino)--}}
{{--                                @if(request()->bino == $bino->id)--}}
{{--                                    <option selected value="{{$bino->id}}">{{ $bino->name }}</option>--}}
{{--                                @else--}}
{{--                                    <option value="{{$bino->id}}">{{ $bino->name }}</option>--}}
{{--                                @endif--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <label for="qavat">Qavat</label>--}}
{{--                        <select name="qavat" id="qavat" class="form-control">--}}
{{--                            <option value="">Qavatni tanlang!</option>--}}
{{--                            @if(request()->qavat == 1)--}}
{{--                                <option selected value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @elseif(request()->qavat == 2)--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option selected value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @elseif(request()->qavat == 3)--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option selected value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @elseif(request()->qavat == 4)--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option selected value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @elseif(request()->qavat == 5)--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option selected value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @elseif(request()->qavat == 6)--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option selected value="6">6</option>--}}
{{--                            @else--}}
{{--                                <option value="1">1</option>--}}
{{--                                <option value="2">2</option>--}}
{{--                                <option value="3">3</option>--}}
{{--                                <option value="4">4</option>--}}
{{--                                <option value="5">5</option>--}}
{{--                                <option value="6">6</option>--}}
{{--                            @endif--}}
{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <label for="xona">Xonalar</label>--}}
{{--                        <select name="xona" id="xona" class="form-control">--}}

{{--                        </select>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3">--}}
{{--                        <label for="title">Nomi</label>--}}
{{--                        <input type="search" class="form-control" name="title" id="title" value="{{ request()->title }}" placeholder="camera nomi...">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="card-footer">--}}
{{--                <button type="submit" class="btn btn-secondary w-100">Qidirish</button>--}}
{{--            </div>--}}
{{--        </form>--}}
    </div>
    <div class="card">
        <div class="card-header d-flex flex-column">
            <div class="d-flex align-items-end justify-content-between">
                <h1>Cameralar Local IP ro'yxati</h1>
                <div>
                    <button id="ft-btn" type="button" class="btn btn-info"><i class="ph-sort-descending me-2"></i>Filter
                    </button>
                    <a id="ft-btn" href="{{ route('camera.localip.checkAll') }}" class="btn btn-warning">Ping</a>
                </div>
            </div>
        </div>
        <div class="card-body row">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Local IP</th>
                    <th>Status</th>
                    <th>PING</th>
                </tr>
                </thead>
                <tbody>
                @foreach($ips as $ip)
                    <tr>
                        <td>{{ $ip->local_ip }}</td>
                        <td>
                            @if($ip->local_ip_check == 1)
                                <span class="text-success">Success</span>
                            @elseif($ip->local_ip_check == 2)
                                <span class="text-danger">Not working</span>
                            @else
                                <span class="text-warning">Please send ping!</span>
                            @endif
                        </td>
                        <td>
                            <a class="btn btn-success" href="{{ route('camera.localip.check',$ip->id) }}">PING</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $ips->appends(request()->all())->links() }}
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/toastr/toastr.min.css') }}">
@endpush
@push('script')
    <script src="{{ asset('assets/toastr/toastr.min.js') }}"></script>
    @if(\Illuminate\Support\Facades\Session::has('success'))
    <script>
        toastr.success('{{ \Illuminate\Support\Facades\Session::get('success') }}');
    </script>
    @endif
@endpush
