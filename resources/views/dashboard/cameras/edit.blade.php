@extends('dashboard.layouts.master')
@section('content')
    <div class="row pt-4">
        <div class="card">
            <div class="card-header text-center">
                <h3 class="card-title">Kamera qo'shish</h3>
            </div>
            <form method="post" action="{{ route('cameras.update',$camera) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body row">
                    <div class="col-md-6 mb-2">
                        <label for="parent">Bo'lim</label>
                        <select required="required" name="sub_id" id="parent"
                                class="form-control section select2bs4">
                            <option value="">Bo'limni tanlang!</option>
                            @foreach($departments as $section)
                                @if($section->id == $camera->sub_id)
                                    <option data-sub_id="{{ $section->id }}" data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" selected value="{{ $section->id }}">{{ $section->name }}</option>
                                @endif
                                @if($section->id != $camera->sub_id)
                                    <option data-sub_id="{{ $section->id }}" style="display: block" data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" value="{{ $section->id }}">{{ $section->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-6 mb-2 sec">
                        <label for="section_id">Sub bo'limlar</label>
                        <select name="section_id" id="section_id" class="form-control section_sub">
                            @if($camera->section_id == 0)
                                <option value="0" selected>Bo'limni tanlang</option>
                            @endif
                            @if($camera->section_id != 0)
                                <option value="0">Bo'limni tanlang</option>
                            @endif
                            @foreach($sub_not_nulls as $section)

                                @if($camera->section_id == $section->id)
                                    <option data-sub-rahbar-id="{{ $section->rahbar_id }}" data-section="{{ $section->sub_id }}" data-sub-bolim-name="{{ $section->name }}" selected value="{{ $section->id }}" >{{ $section->name }}</option>
                                @endif
                                @if($camera->section_id != $section->id)
                                    <option data-sub-rahbar-id="{{ $section->rahbar_id }}" data-section="{{ $section->sub_id }}" data-sub-bolim-name="{{ $section->name }}" value="{{ $section->id }}" >{{ $section->name }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>

                    <div class="col-md-12 mb-2 rahbar">
                        <label for="rahbar_id">Rahbarlar</label>
                        <select name="rahbar_id" id="rahbar_id" class="form-control rahbar_id">
                            @foreach($sub_not_nulls as $section)
                                @if($camera->rahbar_id == $section->rahbar_id)
                                    <option data-section-rahbar="{{ $section->sub_id }}" selected value="{{ $section->rahbar_id }}">{{ $section->rahbar }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>


                    <div class="col-md-4 mb-2">
                        <label for="bino">Binolar</label>
                        <select name="bino" id="bino" class="form-control">
                            @foreach($binolar as $bino)
                                @if($camera->bino == $bino->id)
                                    <option selected value="{{ $bino->id }}">{{ $bino->name }}</option>
                                @endif
                                @if($camera->bino != $bino->id)
                                    <option value="{{ $bino->id }}">{{ $bino->name }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="qavat">Qavatlar</label>
                        <select name="qavat" id="qavat" class="form-control">
                            @for($i=1;$i<=6; $i++)
                                @if($camera->qavat == $i)
                                    <option selected="selected" value="{{ $i }}">{{ $i }}</option>
                                @endif
                                @if($camera->qavat != $i)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endif
                            @endfor
                        </select>
                    </div>
                    <div class="col-md-4 mb-2">
                        <label for="xonalar">Xonalar</label>
                        <select name="xonalar" id="xonalar" class="form-control xonalar">
                            @foreach($xonalar as $xona)
                                @if($camera->xonalar == $xona->xona_nomi)
                                    <option data-xona-id="{{ $xona->id }}" selected value="{{ $xona->xona_nomi }}">{{ $xona->xona_nomi }}</option>
                                @elseif($camera->xonalar != $xona->xona_nomi)
                                    <option data-xona-id="{{ $xona->id }}" value="{{ $xona->xona_nomi }}">{{ $xona->xona_nomi }}</option>
                                @endif
                            @endforeach

                        </select>
                        @error('xonalar')
                        <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                    </div>


                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label for="title">Kamera nomi</label>
                            <input value="{{ $camera->title }}" type="text" name="title"
                                   class="form-control @error('title') is-invalid @enderror" id="title"
                                   placeholder="Kamera nomini kiriting">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                            <label for="link">Kamera linki</label>
                            <input value="{{ $camera->link }}" type="text" name="link"
                                   class="form-control @error('link') is-invalid @enderror" id="link"
                                   placeholder="Kamera linkini kiriting">
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="col-md-3 mb-2">
                        <div class="form-group">
                            <label for="local_ip">Kamera local ip</label>
                            <input value="{{ $camera->local_ip }}" type="text" name="local_ip"
                                   class="form-control @error('local_ip') is-invalid @enderror" id="local_ip"
                                   placeholder="Kamera local ip kiriting">
                            @error('local_ip')
                            <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input name="favorite" value="1" class="custom-control-input custom-control-input-success"
                               type="checkbox" id="customCheckbox4" @if($camera->favorite == 1) checked @else '' @endif>
                        <label for="customCheckbox4" class="custom-control-label">Избранное</label>
                    </div>
                </div>

{{--                @if($camera->rahbarlar)--}}
{{--                    <div class="d-none edit-box">--}}
{{--                        <textarea type="text" name="rahbarlar" id="rahbarlar" class="d-none">--}}
{{--                            {{ $camera->rahbarlar }}--}}
{{--                        </textarea>--}}
{{--                    </div>--}}
{{--                @endif--}}
                <!-- /.card-body -->
                <div id="add_input">
{{--                    <input type="text" name="rahbarlar[id][rector]" id="rahbarlar" class="d-none" value="2555">--}}
                </div>
                <div class="xona_id d-none"></div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-success w-100">Saqlash</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function () {
            var rahbar_bolim_id = '';
            var bolim_name = '';
            var sub_bolim_name = '';
            var rahbar_sub_id = '';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $(".section").change(function () {
                $('.bolim-name').remove();
                $('.sub_bolim_name').remove();
                $(".bolim").remove();
                $('.section_sub').html('');
                $(".edit-box").remove();
                let sub_id = $(this).val();
                let options = $('.section option');
                for (let i = 0; i < options.length; i++) {
                    if (sub_id === options[i].getAttribute('value')) {
                        rahbar_bolim_id = options[i].getAttribute('data-bolim-rahbar-id');
                        bolim_name = options[i].getAttribute('data-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getSubId') }}',
                    method: 'POST',
                    data: {
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option selected value='0'>Bo'limni tanlang</option>";
                        $.each(response.departments, function (index, department) {
                            formoption += "<option data-sub-bolim-name='" + department.name + "' data-sub-rahbar-id='" + department.rahbar_id + "' data-section='" + department.sub_id + "' value='" + department.id + "'>" + (department.name) + "</option>";
                        });

                        $("#add_input").append(`
                            <input name="rahbarlar[id][rahbar]" class="d-none bolim" value="${rahbar_bolim_id}"/>
                            <input name="bolim_name" class="d-none bolim-name" value="${bolim_name}"/>
                            <input name="rahbarlar[id][rector]" class="d-none" value="2555">
                        `);

                        $(".section_sub").append(formoption);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            $(".section_sub").change(function () {
                $('.sub_bolim_name').remove();
                $(".sub__bolim").remove();
                $('.rahbar_id').html('');
                $(".edit-box").remove();
                let id = $(this).val();
                let sub_id = $(this).attr('data-section');
                let options = $('.section_sub option');
                for (let i = 0; i < options.length; i++) {
                    if (id === options[i].getAttribute('value')) {
                        rahbar_sub_id = options[i].getAttribute('data-sub-rahbar-id');
                        sub_bolim_name = options[i].getAttribute('data-sub-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getRahbarId') }}',
                    method: 'POST',
                    data: {
                        id: id,
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        console.log(response.departments)
                        var formoption = "";
                        $.each(response.departments, function (index, department) {
                            formoption += "<option data-section-rahbar='" + department.sub_id + "' value='" + department.rahbar_id + "'>" + department.rahbar + "</option>";
                        });
                        $("#add_input").append(`
                            <input name="rahbarlar[id][rahbar_sub]" class="d-none sub__bolim" value="${rahbar_sub_id}"/>
                            <input name="sub_bolim_name" class="d-none sub_bolim_name" value='${sub_bolim_name}'/>
                            <input name="rahbarlar[id][rector]" class="d-none" value="2555">
                        `);
                        $(".rahbar_id").append(formoption);
                    },

                    // error: function (e) {
                    //     console.log(e);
                    // }
                });
            });

            $('select[name="sub_id"]').change(function () {
                var parent = $(this).val();

                $("select[name='section_id']").val("");
                $(".sec").removeClass('d-none');

                $('select[name="section_id"] option').each(function () {
                    if ($(this).attr('data-section') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

            $('select[name="section_id"]').change(function () {
                var parent = $(this).val();

                $("select[name='rahbar_id']").val("");
                $(".rahbar").removeClass('d-none');

                $('select[name="rahbar_id"] option').each(function () {
                    if ($(this).attr('data-section-rahbar') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });
            var bino = 0;
            $("#bino").change(function () {
                $("#xonalar").html('');
                bino = $(this).val();
                $("#qavat").val("0");
            });
            $("#qavat").change(function () {
                $("#xonalar").html('');
                let qavat = $(this).val();
                $.ajax({
                    url: '{{ route('ajax.xonalar') }}',
                    method: 'POST',
                    data: {
                        bino: bino,
                        qavat: qavat,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option selected value='0'>Xonalarni tanlang</option>";
                        $.each(response.binolar, function (index, xona) {
                            formoption += "<option data-xona-id=" + xona.id + " value='" + xona.xona_nomi + "'>" + xona.xona_nomi + "</option>";
                        });
                        $("#xonalar").append(formoption);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            $("#qavat").change(function (){
                $("#xonalar").html('');
                // $("#bino").html('');
                let bino = $(this).val();
                $("#bino").change(function (){
                    $("#xonalar").html('');
                    let qavat = $(this).val();
                    $.ajax({
                        url: '{{ route('ajax.xonalar') }}',
                        method: 'POST',
                        data: {
                            bino: bino,
                            qavat: qavat,
                        },
                        success: function (response) {
                            var formoption = "";
                            formoption += "<option selected value='0'>Xonalarni tanlang</option>";
                            $.each(response.binolar, function(index, xona) {
                                formoption += "<option data-xona-id="+xona.id+" value='"+xona.id+"'>"+ xona.xona_nomi +"</option>";
                            });

                            $("#xonalar").append(formoption);
                            formoption = '';

                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                });
            });
            $("#xonalar").change(function () {
                $('.xona_id').html('');
                let xona_id = $('option:selected', this).attr('data-xona-id');
                $('.xona_id').append(`
                    <input name="xona_id" value="${xona_id}" type="hidden">
                `);
            });

        });
    </script>
@endpush

