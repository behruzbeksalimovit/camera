<div class="navbar navbar-sm navbar-footer border-top">
    <div class="container-fluid justify-content-center">
        <span>&copy; 1930 - {{ date('Y') }} <a target="_blank" href="https://buxdu.uz">BuxDU</a></span>
    </div>
</div>
