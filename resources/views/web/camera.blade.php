<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="/cameras/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/cameras/styles/style.css" />
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
    />
    <title>BuxDU Camera</title>
</head>
<body>
<nav>
    <div class="container">
        <div class="nav-parent">
            <div class="nav-img">
                <a href="#">
                    <img src="/cameras/logo_uz.svg" width="170" alt="logo"/>
                </a>
            </div>

        </div>
    </div>
</nav>

<section class="cameras">
    <div class="container">
        <h1 class="mb-4 text-center">Onlayn kuzatuv kameralari</h1>
        <div class="row">
{{--            BOSH BINO--}}
{{--            @if($bino === 'bosh-bino')--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['bosh-bino','1']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 1</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['bosh-bino','2']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 2</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['bosh-bino','3']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 3</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['bosh-bino','4']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 4</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['bosh-bino','5']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 5</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--            BOSH BINO--}}
{{--ARM--}}
{{--            @if($bino === 'arm')--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','1']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 1</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','2']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 2</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','3']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 3</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','4']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 4</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','5']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 5</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','6']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 6</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 mb-3">--}}
{{--                    <a href="{{ route('web.camera.fav',['arm','7']) }}">--}}
{{--                        <div class="card">--}}
{{--                            <div class="img-camera">--}}
{{--                                <img src="/cameras/camera.svg" alt="camera">--}}
{{--                            </div>--}}
{{--                            <p>Camera 7</p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            @endif--}}
{{--            ARM--}}





            {{--            BOSH BINO--}}
            @if($bino === 'bosh-bino')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['bosh-bino','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['bosh-bino','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['bosh-bino','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['bosh-bino','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['bosh-bino','5']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 5</p>
                        </div>
                    </a>
                </div>
            @endif
            {{--            BOSH BINO--}}


            {{--                        ARM--}}
            @if($bino === 'arm')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','5']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 5</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','6']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 6</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['arm','7']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 7</p>
                        </div>
                    </a>
                </div>
            @endif
{{--                        ARM--}}
{{--                        KURASH--}}
            @if($bino === 'kurash')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['kurash','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['kurash','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['kurash','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['kurash','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['kurash','5']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 5</p>
                        </div>
                    </a>
                </div>
            @endif
{{--                        KURASH--}}
{{--                        KURASH--}}
            @if($bino === 'boks')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['boks','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['boks','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['boks','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
            @endif
{{--                        BOKS--}}
{{--                        VOLLEYBOL--}}
            @if($bino === 'volleybol')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['volleybol','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['volleybol','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['volleybol','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['volleybol','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
            @endif
{{--                        Volleybol--}}

{{--                        Basketbol--}}
            @if($bino === 'basketbol')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','5']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 5</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','6']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 6</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['basketbol','7']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 7 (YouTube)</p>
                        </div>
                    </a>
                </div>
            @endif
{{--                        Basketbol--}}

{{--                        SPORT MAYDONI--}}
            @if($bino === 'sport')
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','1']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 1</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','2']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 2</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','3']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 3</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','4']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 4</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','5']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 5</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','6']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 6</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','7']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 7</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','8']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Camera 8</p>
                        </div>
                    </a>
                </div>
                <div class="col-md-3 mb-3">
                    <a href="{{ route('web.camera.fav',['sport','mini']) }}">
                        <div class="card">
                            <div class="img-camera">
                                <img src="/cameras/camera.svg" alt="camera">
                            </div>
                            <p>Mini futbol</p>
                        </div>
                    </a>
                </div>
            @endif
{{--            --}}{{--            SPORT MAYDONI--}}
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="/cameras/logo_uz.svg" width="250" alt=""/>
            </div>
            <div class="col-md-4">
                <h4>Bog`lanish</h4>
                <div class="icons">
                    <i class="fa-solid fa-phone"></i>
                    <p>(+998) 65 221-29-14</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-fax"></i>
                    <p>8(365) 221-27-07</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-location-dot"></i>
                    <p>Buxoro sh. M.Iqbol ko`chasi 11-uy</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-envelope"></i>
                    <p>buxdu_rektor@buxdu.uz</p>
                </div>
                <div class="sahifalar">
                    <a href="https://t.me/buxdu_uz"><i class="fa-brands fa-telegram"></i></a>
                    <a href="https://www.facebook.com/buxdu"><i class="fa-brands fa-facebook"></i></a>
                    <a href="https://www.instagram.com/buxdu1/"><i class="fa-brands fa-instagram"></i></a>
                    <a href="https://www.youtube.com/c/BuxDUuzedu"><i class="fa-brands fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Xarita</h4>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3066.994842410637!2d64.42042371487301!3d39.76222420310848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f50060d1c5c0027%3A0xf76b636757c475a5!2z0JHRg9GF0LDRgNGB0LrQuNC5INCT0L7RgdGD0LTQsNGA0YHRgtCy0LXQvdC90YvQuSDQo9C90LjQstC10YDRgdC40YLQtdGC!5e0!3m2!1sru!2s!4v1667027626470!5m2!1sru!2s"
                    width="100%"
                    height="300"
                    style="border: 0"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                ></iframe>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
