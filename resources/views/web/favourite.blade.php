{{--<!doctype html>--}}
{{--<html lang="en">--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <meta name="viewport"--}}
{{--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
{{--    <title>Camera BuxDU</title>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">--}}
{{--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--    <style>--}}

{{--        body {--}}
{{--            background: #fdfbfb;--}}
{{--            background: -webkit-linear-gradient(to right, #fdfbfb, #ebedee);--}}
{{--            background: linear-gradient(to right, #fdfbfb, #ebedee);--}}
{{--            min-height: 100vh;--}}
{{--        }--}}
{{--    </style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<!-- For demo purpose -->--}}
{{--<div class="container py-5">--}}
{{--    --}}{{--    <div class="row text-center text-white">--}}
{{--    --}}{{--        <div class="col-lg-8 mx-auto">--}}
{{--    --}}{{--            <h1 class="display-4">Team Page</h1>--}}
{{--    --}}{{--            <p class="lead mb-0">Using Bootstrap 4 grid and utilities, create a nice team page.</p>--}}
{{--    --}}{{--            <p class="lead">Snippet by<a href="https://bootstrapious.com/snippets" class="text-white">--}}
{{--    --}}{{--                    <u>Bootstrapious</u></a>--}}
{{--    --}}{{--            </p>--}}
{{--    --}}{{--        </div>--}}
{{--    --}}{{--    </div>--}}
{{--</div><!-- End -->--}}


{{--<div class="container">--}}
{{--    <div class="row text-center">--}}
{{--        @if(count($cameras) != 0)--}}
{{--            @foreach($cameras as $camera)--}}
{{--            <!-- Team item -->--}}
{{--                <a class="col-xl-4 col-sm-6 mb-5" href="{{ route('web.camera',$camera->uuid) }}"--}}
{{--                   style="text-decoration: none; color: #0e0f10">--}}
{{--                    <div class="bg-white rounded shadow-sm py-5 px-4"><img--}}
{{--                            src="/camera.png') }}" alt="" width="100"--}}
{{--                            class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">--}}
{{--                        <h4 class="mb-4">{{ $camera->title }}</h4>--}}
{{--                        <h6 class="fw-bolder medium text-uppercase">{{ $camera->bino }} - bino</h6><br>--}}
{{--                        <h6 class="fw-bolder medium text-uppercase">{{ $camera->qavat }} - qavat</h6><br>--}}
{{--                        <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->xonalar }} - xona</h6><br>--}}
{{--                        <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->bolim_name }}</h6><br>--}}
{{--                        @if($camera->sub_bolim_name != null)--}}
{{--                            <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->sub_bolim_name }}</h6><br>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--                <!-- End -->--}}
{{--            @endforeach--}}
{{--        @else--}}
{{--            <div class="col-xl-12 col-sm-12 mb-5">--}}
{{--                <h1>Translyatsiya mavjud emas!</h1>--}}
{{--            </div>--}}
{{--        @endif--}}

{{--    </div>--}}
{{--</div>--}}

{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>--}}
{{--</body>--}}
{{--</html>--}}
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <!-- link css -->
    <link rel="stylesheet" href="/cameras/styles/bootstrap.min.css"/>
    <link rel="stylesheet" href="/cameras/styles/style.css"/>
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
    />
    <title>Buxdu camera</title>
</head>
<body>
<nav>
    <div class="container">
        <div class="nav-parent">
            <div class="nav-img">
                <img src="/cameras/logo_uz.svg" width="170" alt="logo"/>
            </div>

        </div>
    </div>
</nav>

<section class="main" style="background-image: url('/cameras/images/main.png');">
    <div class="img"></div>
    <div class="container">
        <div class="row">
            <div class="main12">
                <div class="main-parent">
                    <p>Buxoro Davlat Unversiteti onlayn kuzatuv web sayti</p>
                    <small
                    >Siz ushbu sayt orqali Buxoro Davlat Universiteti da bo'lib o'tayotgan kasbiy (ijodiy) imtihonlarni
                        onlayn tarzda kuzatib borish imkoniyatiga ega siz!</small
                    >
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cameras">
    <div class="container text-center">
        {{--        @if(count($cameras) != 0)--}}
        <h1 class="mb-4">Onlayn kuzatuv kameralari</h1>
        <div class="row">
            {{--            @foreach($cameras as $camera)--}}
            {{--                    <div class="col-md-3 mb-3">--}}
            {{--                        <a href="{{ route('web.camera',$camera->uuid) }}">--}}
            {{--                            <div class="card">--}}
            {{--                                <div class="img-camera">--}}
            {{--                                    <img src="/cameras/camera.svg') }}" alt="camera">--}}
            {{--                                </div>--}}
            {{--                                <p>{{ $camera->title }}</p>--}}
            {{--                            </div>--}}
            {{--                        </a>--}}
            {{--                    </div>--}}
            {{--                <!-- Team item -->--}}
            {{--                    <a class="col-xl-4 col-sm-6 mb-5" href="{{ route('web.camera',$camera->uuid) }}"--}}
            {{--                       style="text-decoration: none; color: #0e0f10">--}}
            {{--                        <div class="bg-white rounded shadow-sm py-5 px-4"><img--}}
            {{--                                src="/camera.png') }}" alt="" width="100"--}}
            {{--                                class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm">--}}
            {{--                            <h4 class="mb-4">{{ $camera->title }}</h4>--}}
            {{--                            <h6 class="fw-bolder medium text-uppercase">{{ $camera->bino }} - bino</h6><br>--}}
            {{--                            <h6 class="fw-bolder medium text-uppercase">{{ $camera->qavat }} - qavat</h6><br>--}}
            {{--                            <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->xonalar }} - xona</h6><br>--}}
            {{--                            <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->bolim_name }}</h6><br>--}}
            {{--                            @if($camera->sub_bolim_name != null)--}}
            {{--                                <h6 class="fw-bolder medium text-uppercase mb-0">{{ $camera->sub_bolim_name }}</h6><br>--}}
            {{--                            @endif--}}
            {{--                        </div>--}}
            {{--                    </a>--}}
            {{--                    <!-- End -->--}}
            {{--                @endforeach--}}
            {{--            @else--}}
            {{--                <h1 class="mb-4">Translyatsiya mavjud emas!</h1>--}}
            {{--            @endif--}}

            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/bino.png" alt="camera">
                        <p>Bosh bino</p>
                    </div>
                    <div class="d-flex align-content-center justify-content-between">
                        <a style="margin-right: 10px;" href="{{ route('web.cameras','bosh-bino') }}"
                           class="btn-sm btn btn-success">Kameralar</a>
                    </div>
                </div>
            </div>

{{--            <div class="col-md-3 mb-3">--}}
{{--                <div class="card">--}}
{{--                    <div class="d-flex flex-column align-items-center" style="width: 100%">--}}
{{--                        <img style="width: 100px;height: 100px;" src="/bino.png" alt="camera">--}}
{{--                        <p>ARM</p>--}}
{{--                    </div>--}}
{{--                    <div class="d-flex align-content-center justify-content-between">--}}
{{--                        <a style="margin-right: 10px;" href="{{ route('web.cameras','arm') }}"--}}
{{--                           class="btn-sm btn btn-success">Kameralar</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Frame-4.png" alt="camera">
                        <p>Axborot resurs markazi (ARM)</p>
                    </div>
                    <div class="d-flex align-content-center justify-content-between">
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success"
                           href="{{ route('web.cameras','arm') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary" data-bs-toggle="modal" data-bs-target="#arm">--}}
{{--                            Imtihonlar jadvali--}}
{{--                        </button>--}}
                    </div>
                    <div class="modal fade" id="arm" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" style="max-width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5 text-dark" id="exampleModalLabel">Imtihonlar
                                        jadvali</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: left">
                                    <p class="fw-bold">Jurnalistika - <span class="badge bg-success">(08.07.2024 - 09.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Axborot xizmati va jamoatchilik bilan aloqalar - <span
                                            class="badge bg-success">(08.07.2024 - 09.07.2024)</span></p>
                                    <hr>
                                    <p class="fw-bold">Dizayn: intererni loyihalash - <span class="badge bg-success">(10.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Amaliy san’at: amaliy san’at asarlarini ta’mirlash - <span
                                            class="badge bg-success">(11.07.2024)</span></p>
                                    <hr>
                                    <p class="fw-bold">Amaliy san’at: me’moriy yodgorliklar bezagini ta’mirlash - <span
                                            class="badge bg-success">(11.07.2024)</span></p>
                                    <hr>
                                    <p class="fw-bold">Rangtasvir: dastgohli - <span class="badge bg-success">(10.07.2024)</span>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Group 513.png" alt="camera">
                        <p>Kurash zali</p>
                    </div>
                    <div class="d-flex align-content-center justify-content-between">
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success"
                           href="{{ route('web.cameras','kurash') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary" data-bs-toggle="modal" data-bs-target="#kurash">--}}
{{--                            Imtihonlar jadvali--}}
{{--                        </button>--}}
                    </div>
                    <div class="modal fade" id="kurash" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" style="max-width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5 text-dark" id="exampleModalLabel">Imtihonlar
                                        jadvali</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: left">
                                    <p class="fw-bold">Sport faoliyati: erkin kurash - <span class="badge bg-success">(05.07.2024 - 06.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: dzyu-do - <span class="badge bg-success">(07.07.2024 - 08.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: kurash - <span class="badge bg-success">(10.07.2024 - 11.07.2024)</span>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Group 512.png" alt="camera">
                        <p>Boks zali</p>
                    </div>
                    <div class="d-flex align-content-center justify-content-between">
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success"
                           href="{{ route('web.cameras','boks') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary" data-bs-toggle="modal" data-bs-target="#boks">--}}
{{--                            Imtihonlar jadvali--}}
{{--                        </button>--}}
                    </div>
                    <div class="modal fade" id="boks" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" style="max-width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5 text-dark" id="exampleModalLabel">Imtihonlar
                                        jadvali</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: left">
                                    <p class="fw-bold">Adaptiv jismoniy tarbiya va sport - <span
                                            class="badge bg-success">(08.07.2024 - 13.07.2024)</span></p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: boks - <span class="badge bg-success">(05.07.2024 - 07.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: kurash - <span class="badge bg-success">(10.07.2024 - 11.07.2024)</span>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Frame-2.png" alt="camera">
                        <p>Sport maydoni</p>
                    </div>
                    <div class="d-flex align-content-center justify-content-between">
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success"
                           href="{{ route('web.cameras','sport') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary" data-bs-toggle="modal" data-bs-target="#sport">--}}
{{--                            Imtihonlar jadvali--}}
{{--                        </button>--}}
                    </div>
                    <div class="modal fade" id="sport" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" style="max-width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5 text-dark" id="exampleModalLabel">Imtihonlar
                                        jadvali</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: left">
                                    <p class="fw-bold">Sport faoliyati: futbol - <span class="badge bg-success">(08.07.2024 - 10.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: mini-futbol - <span class="badge bg-success">(05.07.2024)</span>
                                    </p>
                                    <hr>
                                    <p class="fw-bold">Sport faoliyati: basketbol - <span class="badge bg-success">(11.07.2024)</span>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Frame-1.png" alt="camera">
                        <p>Voleybol zali</p>
                    </div>
                    <div>
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success" href="{{ route('web.cameras','volleybol') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary" data-bs-toggle="modal" data-bs-target="#volleybol">--}}
{{--                            Imtihonlar jadvali--}}
{{--                        </button>--}}
                    </div>
                    <div class="modal fade" id="volleybol" tabindex="-1" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" style="max-width: 1000px">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5 text-dark" id="exampleModalLabel">Imtihonlar
                                        jadvali</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: left">
                                    <p class="fw-bold">Sport faoliyati: voleybol - <span class="badge bg-success">(09.07.2024 - 10.07.2024)</span>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Yopish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 mb-3">
                <div class="card">
                    <div class="d-flex flex-column align-items-center" style="width: 100%">
                        <img style="width: 100px;height: 100px;" src="/icons/Frame.png" alt="camera">
                        <p>Basketbol zali</p>
                    </div>
                    <div>
                        <a style="margin-right: 10px;" class="btn-sm btn btn-success" href="{{ route('web.cameras','basketbol') }}">Kameralar</a>
{{--                        <button type="button" class="btn-sm btn btn-primary">Imtihonlar jadvali</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <img src="/cameras/logo_uz.svg" width="250" alt=""/>
            </div>
            <div class="col-md-4">
                <h4>Bog`lanish</h4>
                <div class="icons">
                    <i class="fa-solid fa-phone"></i>
                    <p>(+998) 65 221-29-14</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-fax"></i>
                    <p>8(365) 221-27-07</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-location-dot"></i>
                    <p>Buxoro sh. M.Iqbol ko`chasi 11-uy</p>
                </div>
                <div class="icons">
                    <i class="fa-solid fa-envelope"></i>
                    <p>buxdu_rektor@buxdu.uz</p>
                </div>
                <div class="sahifalar">
                    <a href="https://t.me/buxdu_uz"><i class="fa-brands fa-telegram"></i></a>
                    <a href="https://www.facebook.com/buxdu"><i class="fa-brands fa-facebook"></i></a>
                    <a href="https://www.instagram.com/buxdu1/"><i class="fa-brands fa-instagram"></i></a>
                    <a href="https://www.youtube.com/c/BuxDUuzedu"><i class="fa-brands fa-youtube"></i></a>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Xarita</h4>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3066.994842410637!2d64.42042371487301!3d39.76222420310848!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f50060d1c5c0027%3A0xf76b636757c475a5!2z0JHRg9GF0LDRgNGB0LrQuNC5INCT0L7RgdGD0LTQsNGA0YHRgtCy0LXQvdC90YvQuSDQo9C90LjQstC10YDRgdC40YLQtdGC!5e0!3m2!1sru!2s!4v1667027626470!5m2!1sru!2s"
                    width="100%"
                    height="300"
                    style="border: 0"
                    allowfullscreen=""
                    loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"
                ></iframe>
            </div>
        </div>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz"
        crossorigin="anonymous"></script>

</body>
</html>
