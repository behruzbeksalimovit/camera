@extends('users.layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h1 class="text-center">{{ $camera->title }}</h1>
            <div class="d-flex align-items-center justify-content-center">
                <p class="fs-1 me-3"><span class="fw-bold">Bino:</span> {{ $camera->bino }}</p>
                <p class="fs-1 me-3"><span class="fw-bold">Qavat:</span> {{ $camera->qavat }}</p>
                <p class="fs-1 me-3"><span class="fw-bold">Xona:</span> {{ $camera->xonalar }}</p>
            </div>
        </div>
        <div class="card-body row h-100">
            <iframe style="width: 100%; height: 60vh"
                    {{--                    src="https://www.youtube.com/embed/tgbNymZ7vqY"--}}
                    src="http://go.camera.buxdu.uz/stream/player2/{{$camera->uuid}}"
            >
            </iframe>
        </div>
    </div>
@endsection

