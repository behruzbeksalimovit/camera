@extends('users.layouts.master')
@section('content')
    <div id="filter-card" class="card shadow-lg">
        <form action="{{ route('users.index') }}" method="get">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <label for="bino">Bino</label>
                        <select name="bino" id="bino" class="form-control">
                            <option value="">Binoni tanlang!</option>
                            @foreach($binolar as $bino)
                                @if(request()->bino == $bino->id)
                                    <option selected value="{{$bino->id}}">{{ $bino->name }}</option>
                                @else
                                    <option value="{{$bino->id}}">{{ $bino->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="qavat">Qavat</label>
                        <select name="qavat" id="qavat" class="form-control">
                            <option value="">Qavatni tanlang!</option>
                            @if(request()->qavat == 1)
                                <option selected value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            @elseif(request()->qavat == 2)
                                <option value="1">1</option>
                                <option selected value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            @elseif(request()->qavat == 3)
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option selected value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            @elseif(request()->qavat == 4)
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option selected value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            @elseif(request()->qavat == 5)
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option selected value="5">5</option>
                                <option value="6">6</option>
                            @elseif(request()->qavat == 6)
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option selected value="6">6</option>
                            @else
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="xona">Xonalar</label>
                        <select name="xona" id="xona" class="form-control">

                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="title">Nomi</label>
                        <input type="search" class="form-control" name="title" id="title" value="{{ request()->title }}" placeholder="camera nomi...">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-secondary w-100">Qidirish</button>
            </div>
        </form>
    </div>
    <div class="card">
        <div class="card-header d-flex flex-column">
            <div class="d-flex align-items-center justify-content-between">
                <h1>Cameralar ro'yxati</h1>
                <button id="ft-btn" type="button" class="btn btn-info"><i class="ph-sort-descending me-2"></i>Filter</button>
            </div>
        </div>
        <div class="card-body row">
            @foreach($cameras as $camera)
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <h3>
                                <a>{{ $camera->title }}</a>
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-2">
                            <h2>Bino: {{ $camera->bino }}</h2>
                            <h2>Qavat: {{ $camera->qavat }}</h2>
                            <h2>Xona: {{ $camera->xonalar }}</h2>
                        </div>
                        <div class="card-footer text-end">
                            <a href="{{ route('users.camera.show',$camera->uuid) }}" class="btn btn-sm btn-primary">Ko'rish</a>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            @endforeach
        </div>
        <div class="card-footer">
            {{ $cameras->appends(request()->all())->links() }}
        </div>
    </div>
@endsection
@push('user_script')
    <script>
        $(document).ready(function () {
            var qavat = '{{ request()->qavat }}';
            var bino = '{{ request()->bino }}';

            @if(count(request()->all()) == 0)
            $('#filter-card').hide();
            @else
            $('#filter-card').show();
            @endif
            $("#ft-btn").click(function () {
                $('#filter-card').slideToggle(1000);
            });


            $.ajaxSetup({
                headers:
                    { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
            });

            $("#qavat").change(function (){
                qavat = $(this).val();
                $("#xona").html('');
                $.ajax({
                    url: '{{ route('ajax.xonalar') }}',
                    method: 'POST',
                    data: {
                        bino: bino,
                        qavat: qavat
                    },
                    success: function (response) {
                        console.log(response);
                        $("#xona").append(`
                            <option value="">Xonani tanlang!</option>
                        `);
                        $.each(response.binolar, function (index, xona) {
                            $("#xona").append(`
                                <option value="${xona.xona_nomi}">${xona.xona_nomi}</option>
                            `);
                        });
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            $("#bino").change(function (){
                bino = $(this).val();
                $("#xona").html('');
                $.ajax({
                    url: '{{ route('ajax.xonalar') }}',
                    method: 'POST',
                    data: {
                        bino: bino,
                        qavat: qavat
                    },
                    success: function (response) {
                        console.log(response);
                        $("#xona").append(`
                            <option value="">Xonani tanlang!</option>
                        `);
                        $.each(response.binolar, function (index, xona) {
                            $("#xona").append(`
                                <option value="${xona.xona_nomi}">${xona.xona_nomi}</option>
                            `);
                        });
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });
        });
    </script>
@endpush
